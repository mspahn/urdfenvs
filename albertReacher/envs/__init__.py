from albertReacher.envs.tor import AlbertReacherTorEnv
from albertReacher.envs.vel import AlbertReacherVelEnv
from albertReacher.envs.acc import AlbertReacherAccEnv
