from pandaReacher.envs.tor import PandaReacherTorEnv
from pandaReacher.envs.vel import PandaReacherVelEnv
from pandaReacher.envs.acc import PandaReacherAccEnv
